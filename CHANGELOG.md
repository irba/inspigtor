version 0.0.5
--------------
* UPDATED work on windows installer

version 0.0.4
--------------
* UPDATED work on windows installer

version 0.0.3
--------------
* UPDATED work on windows installer

version 0.0.2
--------------
* UPDATED refactored GUI
* ADDED   code for computing statistics
* ADDED   launching script

version 0.0.1
--------------
* ADDED   GUI
* UPDATED PiCCO2 file reader with methods for computing valid and record intervals

version 0.0.0
--------------
* ADDED   PiCCO2 file reader
* ADDED   code, pylint and gitignore files